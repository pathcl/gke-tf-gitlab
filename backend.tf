terraform {
  backend "gcs" {
    bucket      = "gke-terraform-demo"
    credentials = "${file("./creds/serviceaccount.json")}"
  }
}
