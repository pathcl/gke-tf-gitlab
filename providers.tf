provider "google" {
  credentials = "${file("./creds/serviceaccount.json")}"
  project     = "weather-4ad4a"
  region      = "us-west1-c"
}
