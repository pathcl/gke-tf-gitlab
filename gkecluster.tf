resource "google_container_cluster" "gke-cluster" {
  name               = "my-first-gke-cluster"
  network            = "default"
  zone               = "us-west1-c"
  initial_node_count = 3
}

resource "google_container_node_pool" "node-pool" {
  name               = "default-pool"
  zone               = "us-west1-c"
  cluster            = "${google_container_cluster.gke-cluster.name}"
  node_count         = 1
}